param([string]$path)

function checkStatus($filePath)
    {
        write-host "[ACTION][FILECHECK] Checking if" $filePath "is locked"
        $fileInfo = New-Object System.IO.FileInfo $filePath

        try 
        {
            $fileStream = $fileInfo.Open( [System.IO.FileMode]::Open, [System.IO.FileAccess]::Read, [System.IO.FileShare]::Read )
            write-host "[ACTION][FILEAVAILABLE]" $filePath
			$fileStream.Close();
            return $true
        }
        catch
        {
            write-host "[ACTION][FILELOCKED] $filePath is locked"
            return $false
        }
    }
	
do {
	Start-Sleep -m 100
} until (checkStatus($path))