<#
File Monitor monitors the C:\NAXML\Export\Xchg folder and mirrors its contents to 
C:\NAXML\ExportBOS2\Xchg, C:\NAXML\ExportPS\Xchg, and C:\NAXML\ExportTG\Xchg directories
#>
# Monitored directory
$global:folder = 'C:\NAXML\Export\Xchg'

# Mirroring Directories
$global:destination1 = 'C:\NAXML\ExportBOS2\Xchg'
$global:destination2 = 'C:\NAXML\ExportPS\Xchg'
$global:destination3 = 'C:\NAXML\ExportTG\Xchg'
$filter = '*.*'

# checkStatus determines when a file transfer is complete.
function checkStatus($filePath)
{
	write-host "[ACTION][FILECHECK] Checking if" $filePath "is locked"
	$fileInfo = New-Object System.IO.FileInfo $filePath

	try 
	{
		$fileStream = $fileInfo.Open( [System.IO.FileMode]::Open, [System.IO.FileAccess]::Read, [System.IO.FileShare]::Read )
		write-host "[ACTION][FILEAVAILABLE]" $filePath
		$fileStream.Close();
		return $true
	}
	catch
	{
		write-host "[ACTION][FILELOCKED] $filePath is locked"
		return $false
	}
}

# FileSystemWatcher object watches for changes in FileName and LastWrite
$fsw = New-Object IO.FileSystemWatcher $folder, $filter -Property @{
 IncludeSubdirectories = $true
 NotifyFilter = [IO.NotifyFilters]'FileName, LastWrite'
}

# Delete any existing registered event.
Unregister-Event -SourceIdentifier FileCreated -ErrorAction 

# Event watching for file changes
$onCreated = Register-ObjectEvent $fsw Created -SourceIdentifier FileCreated -Action {
 $path = $Event.SourceEventArgs.FullPath
 $name = $Event.SourceEventArgs.Name
 $changeType = $Event.SourceEventArgs.ChangeType
 $timeStamp = $Event.TimeGenerated 
 Write-Host "The file '$name' was $changeType at $timeStamp"
	
 # Wait until file transfered completed.
 do {
	Start-Sleep -m 100
 } until (checkStatus($path))

 # Copy to the mirrored directories
 $copyBlock = {
	Param($name, $folder, $destination)
	echo f | xcopy $folder\$name  $destination\$name /y /i /e /s
 }
 Start-Job -scriptblock $copyBlock -ArgumentList $name, $global:folder, $global:destination1
 Start-Job -scriptblock $copyBlock -ArgumentList $name, $global:folder, $global:destination2
 Start-Job -scriptblock $copyBlock -ArgumentList $name, $global:folder, $global:destination3
}